﻿using System;
using System.Collections.Generic;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SendGRPC;
using UHF;
using Converter;
using Errors;
using Google.Protobuf;
using System.Linq;

namespace TimingSystemNCore
{

    public partial class RaceUserControl : UserControl
    {

        public class Sportsmen
        {
            public string firstName;
            public string lastName;
            public string time;
            public string rfid;
            public int userID;
            public Sportsmen(string firstName, string lastName, string time, string rfid, int userID)
            {
                this.firstName = firstName;
                this.lastName = lastName;
                this.time = time;
                this.rfid = rfid;
                this.userID = userID;
            }
        }

        private List<Sportsmen> spsList = new List<Sportsmen>();

        private void init(int count)
        {
            for (int i = 0; i < count; i++)
            {
                spsList.Add(new Sportsmen(null, null, null, null, i));
            }
        }

       
        private bool fIsInventoryScan;
        private byte scantime = 0;
        private DateTime startTime;
        private DateTime checkTime;
        private TimeSpan resultTime;
        private TimeSpan writeInhibitInterval = TimeSpan.FromSeconds(5);
        private Dictionary<string, List<TimeSpan>> resultData = new Dictionary<string, List<TimeSpan>>();
        private Dictionary<string, TimeSpan> bufferData = new Dictionary<string, TimeSpan>();
        private List<UserResult> personsResult = new List<UserResult>();
        private UserResult personResults = new UserResult();
        private List<UserData> usersData = new List<UserData>();
        private int competitionID;
        private Dictionary<string, int> competitionsNameId = new Dictionary<string, int>();

        public RaceUserControl()
        {
            InitializeComponent();
        }
        private void buttonSendRaceData_Click(object sender, EventArgs e)
        {   
            try
            {
                Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);
                var client = new Greeter.GreeterClient(channel);

                //Генерирует результаты гонки
                for (int i = 0; i < 10; i++)
                {
                    personResults.RFID = "1";
                    personResults.Time = "123";
                    personsResult.Add(personResults);
                }

                var reply = client.SendRaceData(new SendRaceDataReqest {CompetitionID = competitionID, UsersResult = { personsResult } });
                if (reply.IsSend)
                {
                    richTextBoxLogs.AppendText("Send data success!" + Environment.NewLine);
                }
                else
                {
                    richTextBoxLogs.AppendText("Send data error!" + Environment.NewLine);
                }
                channel.ShutdownAsync().Wait();
            }
            catch (Exception ex)
            {
                richTextBoxLogs.AppendText("Send data error!" + $"Exception {ex.Message}" + Environment.NewLine);
            }

        }

        private void btnGetRaceData_Click(object sender, EventArgs e)
        {
            try
            {
                Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);
                var client = new Greeter.GreeterClient(channel);
                var reply = client.GetRaceData(new GetRaceDataRequest { CompetitionID = competitionID });

                foreach(UserData User in reply.UsersData)
                {   //Добавляем информацию для гонки туда куда надо
                    if(!resultData.ContainsKey(User.RFID))
                    {
                        resultData.Add(User.RFID, new List<TimeSpan>());
                        bufferData.Add(User.RFID, new TimeSpan());
                        richTextBoxLogs.AppendText("EPC: " + User.RFID + " added." + Environment.NewLine);
                    }
                    usersData.Add(User);
                }
                richTextBoxLogs.AppendText("Get data success." + Environment.NewLine);
                channel.ShutdownAsync().Wait();
            }
            catch (Exception ex)
            {
                richTextBoxLogs.AppendText("Get data error!"+ $"Exception: {ex.Message}" + Environment.NewLine);
            }

        }

        private void btnStartRace_Click(object sender, EventArgs e)
        {
            ScanTimer.Enabled = !ScanTimer.Enabled;

            if (ScanTimer.Enabled)
            {
                startTime = DateTime.Now;
                dataGridView1.Rows.Clear();
                scantime = Convert.ToByte(commboBoxScanTime.SelectedIndex + 3);
                fIsInventoryScan = false;

                btnStartRace.Text = "Stop";
            }
            else
            {
                btnStartRace.Text = "Start";
            }
        }

        private void inventory()
        {
            byte Ant = 0;
            int CardNum = 0;
            int Totallen = 0;
            int EPClen, m;
            byte[] EPC = new byte[50000];
            int CardIndex;
            string temps, temp;
            temp = "";
            string sEPC;
            byte MaskMem = 0;
            byte[] MaskAdr = new byte[2];
            byte MaskLen = 0;
            byte[] MaskData = new byte[100];
            byte MaskFlag = 0;
            byte AdrTID = 0;
            byte LenTID = 0;
            DataGridViewRow rows = new DataGridViewRow();


            ErrorsCatcher.errorName = RWDev.Inventory_G2(ref RWDev.comAddress, RWDev.QValue, RWDev.session, MaskMem, MaskAdr, MaskLen, MaskData, 
                                            MaskFlag, AdrTID, LenTID, RWDev.TIDFlag, RWDev.target, RWDev.inAnt, scantime, RWDev.fastFlag, 
                                            EPC, ref Ant, ref Totallen, ref CardNum, RWDev.frmComPortIndex);

            checkTime = DateTime.Now;
            resultTime = checkTime - startTime;

            if ((ErrorsCatcher.errorName == 1) | (ErrorsCatcher.errorName == 2) | (ErrorsCatcher.errorName == 3) | (ErrorsCatcher.errorName == 4))
            {
                byte[] daw = new byte[Totallen];
                Array.Copy(EPC, daw, Totallen);
                temps = ConverterTo.ByteArrayToHexString(daw);
                m = 0;
                if (CardNum == 0) return;

                for (CardIndex = 0; CardIndex < CardNum; CardIndex++)
                {
                    EPClen = daw[m] + 1;
                    temp = temps.Substring(m * 2 + 2, EPClen * 2);
                    sEPC = temp.Substring(0, temp.Length - 2);
                    string RSSI = Convert.ToInt32(temp.Substring(temp.Length - 2, 2), 16).ToString();
                    m = m + EPClen + 1;
                    if (sEPC.Length != (EPClen - 1) * 2)
                    {
                        return;
                    }

                    string[] arr = new string[4];
                    int Lap = 0;
                    Lap = 0; // resultData[sEPC].Count + 1;
                    arr[0] = (dataGridView1.RowCount + 1).ToString();
                    arr[1] = sEPC;
                    arr[2] = "00:00:00";  
                    arr[3] = Lap.ToString();
                    /*if (resultData.ContainsKey(sEPC) && bufferData.ContainsKey(sEPC))
                    {
                        if (resultData[sEPC].Count == 0)
                        {
                            resultData[sEPC].Add(resultTime);
                            arr[2] = resultTime.ToString();

                            dataGridView1.Rows.Insert(dataGridView1.RowCount, arr);
                            richTextBoxLogs.AppendText(resultTime.ToString() + " " + sEPC + Environment.NewLine);
                        }
                        else if ((resultTime - bufferData[sEPC]) > writeInhibitInterval)
                        {
                            resultData[sEPC].Add(resultTime);
                            arr[2] = resultTime.ToString();

                            dataGridView1.Rows.Insert(dataGridView1.RowCount, arr);
                            richTextBoxLogs.AppendText(resultTime.ToString() + " " + sEPC + Environment.NewLine);                            
                        }
                        bufferData[sEPC] = resultTime;
                    }*/
                    richTextBoxLogs.AppendText(resultTime.ToString() + " " + sEPC + Environment.NewLine);
                }
            }
        }

        private void ScanTimer_Tick(object sender, EventArgs e)
        {
            if (fIsInventoryScan)
                return;
            fIsInventoryScan = true;
            inventory();
            fIsInventoryScan = false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void labelScanTime_Click(object sender, EventArgs e)
        {

        }

        private void RaceUserControl_Load(object sender, EventArgs e)
        {
            int i = 0;
            for (i = 0x03; i <= 0xff; i++)
                commboBoxScanTime.Items.Add(Convert.ToString(i) + "*100ms");
            commboBoxScanTime.SelectedIndex = 7;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            richTextBoxLogs.Clear();
        }

        private void buttonFindCompetitions_Click(object sender, EventArgs e)
        {
            comboBoxCompetitions.Items.Clear();
            competitionsNameId.Clear();
            try
            {
                Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);
                var client = new Greeter.GreeterClient(channel);
                var reply = client.GetCompetitions(new CompetitionsRequest { IsGet = true });
                foreach (Competition c in reply.Competitions)
                {
                    richTextBoxLogs.AppendText(c.CompitionName + Environment.NewLine);
                    comboBoxCompetitions.Items.Add(c.CompitionName);
                    competitionsNameId.Add(c.CompitionName, c.CompetitionID);
                }
                comboBoxCompetitions.SelectedIndex = 0;
                richTextBoxLogs.AppendText("find competitions success!" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                richTextBoxLogs.AppendText("Can't find competitions" + $"Exception: {ex.Message}" + Environment.NewLine);
            }
        }
    }
}

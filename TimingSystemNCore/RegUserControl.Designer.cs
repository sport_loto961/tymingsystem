﻿namespace TimingSystemNCore
{
    partial class RegUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFindPerson = new System.Windows.Forms.Button();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.labelID = new System.Windows.Forms.Label();
            this.buttonWrite = new System.Windows.Forms.Button();
            this.groupBoxFindPerson = new System.Windows.Forms.GroupBox();
            this.comboBoxCompetitions = new System.Windows.Forms.ComboBox();
            this.labelCompetitions = new System.Windows.Forms.Label();
            this.groupBoxWriteEPC = new System.Windows.Forms.GroupBox();
            this.labelOperationRecords = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.richTextBoxLogs = new System.Windows.Forms.RichTextBox();
            this.groupBoxFindCompetitions = new System.Windows.Forms.GroupBox();
            this.buttonFindCompetitions = new System.Windows.Forms.Button();
            this.groupBoxFindPerson.SuspendLayout();
            this.groupBoxWriteEPC.SuspendLayout();
            this.groupBoxFindCompetitions.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnFindPerson
            // 
            this.btnFindPerson.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.btnFindPerson.FlatAppearance.BorderSize = 0;
            this.btnFindPerson.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFindPerson.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnFindPerson.ForeColor = System.Drawing.SystemColors.Control;
            this.btnFindPerson.Location = new System.Drawing.Point(138, 164);
            this.btnFindPerson.Name = "btnFindPerson";
            this.btnFindPerson.Size = new System.Drawing.Size(189, 38);
            this.btnFindPerson.TabIndex = 1;
            this.btnFindPerson.Text = "Find";
            this.btnFindPerson.UseVisualStyleBackColor = false;
            this.btnFindPerson.Click += new System.EventHandler(this.btnFindPerson_Click);
            // 
            // textBoxID
            // 
            this.textBoxID.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxID.Location = new System.Drawing.Point(138, 121);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(189, 27);
            this.textBoxID.TabIndex = 2;
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelID.Location = new System.Drawing.Point(101, 124);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(31, 21);
            this.labelID.TabIndex = 2;
            this.labelID.Text = "ID:";
            // 
            // buttonWrite
            // 
            this.buttonWrite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonWrite.FlatAppearance.BorderSize = 0;
            this.buttonWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWrite.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonWrite.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonWrite.Location = new System.Drawing.Point(21, 51);
            this.buttonWrite.Name = "buttonWrite";
            this.buttonWrite.Size = new System.Drawing.Size(189, 38);
            this.buttonWrite.TabIndex = 1;
            this.buttonWrite.Text = "Write";
            this.buttonWrite.UseVisualStyleBackColor = false;
            this.buttonWrite.Click += new System.EventHandler(this.buttonWrite_Click);
            // 
            // groupBoxFindPerson
            // 
            this.groupBoxFindPerson.Controls.Add(this.comboBoxCompetitions);
            this.groupBoxFindPerson.Controls.Add(this.btnFindPerson);
            this.groupBoxFindPerson.Controls.Add(this.labelCompetitions);
            this.groupBoxFindPerson.Controls.Add(this.labelID);
            this.groupBoxFindPerson.Controls.Add(this.textBoxID);
            this.groupBoxFindPerson.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxFindPerson.Location = new System.Drawing.Point(286, 54);
            this.groupBoxFindPerson.Name = "groupBoxFindPerson";
            this.groupBoxFindPerson.Size = new System.Drawing.Size(344, 272);
            this.groupBoxFindPerson.TabIndex = 3;
            this.groupBoxFindPerson.TabStop = false;
            this.groupBoxFindPerson.Text = "FInd person";
            // 
            // comboBoxCompetitions
            // 
            this.comboBoxCompetitions.FormattingEnabled = true;
            this.comboBoxCompetitions.Location = new System.Drawing.Point(138, 74);
            this.comboBoxCompetitions.Name = "comboBoxCompetitions";
            this.comboBoxCompetitions.Size = new System.Drawing.Size(189, 24);
            this.comboBoxCompetitions.TabIndex = 3;
            // 
            // labelCompetitions
            // 
            this.labelCompetitions.AutoSize = true;
            this.labelCompetitions.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelCompetitions.Location = new System.Drawing.Point(13, 74);
            this.labelCompetitions.Name = "labelCompetitions";
            this.labelCompetitions.Size = new System.Drawing.Size(119, 21);
            this.labelCompetitions.TabIndex = 2;
            this.labelCompetitions.Text = "Competitions:";
            this.labelCompetitions.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBoxWriteEPC
            // 
            this.groupBoxWriteEPC.Controls.Add(this.buttonWrite);
            this.groupBoxWriteEPC.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxWriteEPC.Location = new System.Drawing.Point(40, 198);
            this.groupBoxWriteEPC.Name = "groupBoxWriteEPC";
            this.groupBoxWriteEPC.Size = new System.Drawing.Size(229, 128);
            this.groupBoxWriteEPC.TabIndex = 4;
            this.groupBoxWriteEPC.TabStop = false;
            this.groupBoxWriteEPC.Text = "Write";
            // 
            // labelOperationRecords
            // 
            this.labelOperationRecords.AutoSize = true;
            this.labelOperationRecords.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelOperationRecords.Location = new System.Drawing.Point(31, 408);
            this.labelOperationRecords.Name = "labelOperationRecords";
            this.labelOperationRecords.Size = new System.Drawing.Size(129, 17);
            this.labelOperationRecords.TabIndex = 3;
            this.labelOperationRecords.Text = "Operation records:";
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.Control;
            this.btnRefresh.Location = new System.Drawing.Point(562, 402);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(96, 23);
            this.btnRefresh.TabIndex = 1;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // richTextBoxLogs
            // 
            this.richTextBoxLogs.Location = new System.Drawing.Point(12, 428);
            this.richTextBoxLogs.Name = "richTextBoxLogs";
            this.richTextBoxLogs.Size = new System.Drawing.Size(658, 96);
            this.richTextBoxLogs.TabIndex = 5;
            this.richTextBoxLogs.Text = "";
            this.richTextBoxLogs.TextChanged += new System.EventHandler(this.richTextBoxLogs_TextChanged);
            // 
            // groupBoxFindCompetitions
            // 
            this.groupBoxFindCompetitions.Controls.Add(this.buttonFindCompetitions);
            this.groupBoxFindCompetitions.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxFindCompetitions.Location = new System.Drawing.Point(40, 54);
            this.groupBoxFindCompetitions.Name = "groupBoxFindCompetitions";
            this.groupBoxFindCompetitions.Size = new System.Drawing.Size(229, 128);
            this.groupBoxFindCompetitions.TabIndex = 6;
            this.groupBoxFindCompetitions.TabStop = false;
            this.groupBoxFindCompetitions.Text = "Find competitions";
            // 
            // buttonFindCompetitions
            // 
            this.buttonFindCompetitions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonFindCompetitions.FlatAppearance.BorderSize = 0;
            this.buttonFindCompetitions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFindCompetitions.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonFindCompetitions.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonFindCompetitions.Location = new System.Drawing.Point(21, 49);
            this.buttonFindCompetitions.Name = "buttonFindCompetitions";
            this.buttonFindCompetitions.Size = new System.Drawing.Size(189, 38);
            this.buttonFindCompetitions.TabIndex = 1;
            this.buttonFindCompetitions.Text = "Find";
            this.buttonFindCompetitions.UseVisualStyleBackColor = false;
            this.buttonFindCompetitions.Click += new System.EventHandler(this.buttonFindCompetitions_Click);
            // 
            // RegUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxFindCompetitions);
            this.Controls.Add(this.richTextBoxLogs);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.labelOperationRecords);
            this.Controls.Add(this.groupBoxWriteEPC);
            this.Controls.Add(this.groupBoxFindPerson);
            this.Name = "RegUserControl";
            this.Size = new System.Drawing.Size(680, 536);
            this.Load += new System.EventHandler(this.RegUserControl_Load);
            this.groupBoxFindPerson.ResumeLayout(false);
            this.groupBoxFindPerson.PerformLayout();
            this.groupBoxWriteEPC.ResumeLayout(false);
            this.groupBoxFindCompetitions.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFindPerson;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.Button buttonWrite;
        private System.Windows.Forms.GroupBox groupBoxFindPerson;
        private System.Windows.Forms.Label labelOutput;
        private System.Windows.Forms.GroupBox groupBoxWriteEPC;
        private System.Windows.Forms.Label labelOperationRecords;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.RichTextBox richTextBoxLogs;
        private System.Windows.Forms.Label labelCompetitions;
        private System.Windows.Forms.GroupBox groupBoxFindCompetitions;
        private System.Windows.Forms.Button buttonFindCompetitions;
        private System.Windows.Forms.ComboBox comboBoxCompetitions;
    }
}

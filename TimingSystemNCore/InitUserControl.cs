﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Errors;
using Converter;
using UHF;

namespace TimingSystemNCore
{
    public partial class InitUserControl : UserControl
    {
        
        public InitUserControl()
        {
            InitializeComponent();
        }

        //private int errorName;

        private void btnConnectReader_Click(object sender, EventArgs e)
        {
            int portNum = comboBoxCOM.SelectedIndex + 1;

            byte baudRate = Convert.ToByte(comboBoxBaud.SelectedIndex);
            if (baudRate > 2)
                baudRate = Convert.ToByte(baudRate + 2);
            int portHandle = 0;


            ErrorsCatcher.errorName = RWDev.OpenComPort(portNum, ref RWDev.comAddress, baudRate, ref portHandle);
            if (ErrorsCatcher.errorName != 0)
            {
                string strLog = "Connection failed: " + ErrorsCatcher.GetReturnCodeDesc(ErrorsCatcher.errorName) + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
                richTextBoxLogs.ScrollToCaret();
                return;
            }
            else
            {
                RWDev.frmComPortIndex = portHandle;
                string strLog = "Connected  " + comboBoxCOM.Text + "@" + comboBoxBaud.Text + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
            }
            EnabledForm();
            btnConnectReader.Enabled = false;
            btnDisconnectReader.Enabled = true;
        }

        private void btnDisconnectReader_Click(object sender, EventArgs e)
        {
            if (RWDev.frmComPortIndex > 0)
                RWDev.CloseSpecComPort(RWDev.frmComPortIndex);
            string strLog = "Dissconected" + Environment.NewLine;
            richTextBoxLogs.AppendText(strLog);
            btnConnectReader.Enabled = true;
            btnDisconnectReader.Enabled = false;
            DisabledForm();
        }

        private void labeldBm_Click(object sender, EventArgs e)
        {

        }

        private void btnGetBuffLength_Click(object sender, EventArgs e)
        {
            byte SaveLen = 0;
            ErrorsCatcher.errorName = RWDev.GetSaveLen(ref RWDev.comAddress, ref SaveLen, RWDev.frmComPortIndex);
            if (ErrorsCatcher.errorName == 0)
            {
                
                if (SaveLen == 0)
                    radioButton128bitBuff.Checked = true;
                else
                    radioButton496bitBuff.Checked = true;
                string strLog = "Get buffer EPC/TID length success " + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
            }
            else
            {
                string strLog = "Get buffer EPC/TID length failed " + ErrorsCatcher.GetReturnCodeDesc(ErrorsCatcher.errorName) + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
                return;
            }
        }

        private void btnSetAdr_Click(object sender, EventArgs e)
        {
            byte aNewComAdr = Convert.ToByte(textBoxReadAddress.Text, 16);
            ErrorsCatcher.errorName = RWDev.SetAddress(ref RWDev.comAddress, aNewComAdr, RWDev.frmComPortIndex);
            if (ErrorsCatcher.errorName != 0)
            {
                string strLog = "Set address failed " + ErrorsCatcher.GetReturnCodeDesc(ErrorsCatcher.errorName) + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
                return;
            }
            else
            {
                string strLog = "Address " + textBoxReadAddress.Text + " set" + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
            }
        }

        private void EnabledForm()
        {
            groupBoxReaderAddress.Enabled = true;
            groupBoxPower.Enabled = true;
            groupBoxGPIOOperation.Enabled = true;
            groupBoxRedaerSerialNumber.Enabled = true;
            groupBoxDBM.Enabled = true;
            groupBoxRegion.Enabled = true;
            groupBoxBuffLength.Enabled = true;
            groupBoxBeep.Enabled = true;
            groupBoxFirmwareVersion.Enabled = true;
            RWDev.readerInit = true;
        }

        private void DisabledForm()
        {
            textBoxFirmwareVersion.Text = "";
            textBoxReaderSerialNumber.Text = "";
            groupBoxReaderAddress.Enabled = false;
            groupBoxPower.Enabled = false;
            groupBoxGPIOOperation.Enabled = false;
            groupBoxRedaerSerialNumber.Enabled = false;
            groupBoxDBM.Enabled = false;
            groupBoxRegion.Enabled = false;
            groupBoxBuffLength.Enabled = false;
            groupBoxBeep.Enabled = false;
            groupBoxFirmwareVersion.Enabled = false;
            RWDev.readerInit = false;
        }

        private void InitUserControl_Load(object sender, EventArgs e)
        {
            DisabledForm();
            comboBoxBaud.SelectedIndex = 3;
            comboBoxCOM.SelectedIndex = 0;
            radioButtonEUband.Checked = true;
            comboBoxSetBaudRate.SelectedIndex = 3;
            comboBoxPower.SelectedIndex = 26;
        }

        private void buttonGPIOSet_Click(object sender, EventArgs e)
        {
            byte OutputPin = 0;
            if (checkBoxOUT1.Checked)
                OutputPin = Convert.ToByte(OutputPin | 0x01);
            if (checkBoxOUT2.Checked)
                OutputPin = Convert.ToByte(OutputPin | 0x02);
            ErrorsCatcher.errorName = RWDev.SetGPIO(ref RWDev.comAddress, OutputPin, RWDev.frmComPortIndex);
            if (ErrorsCatcher.errorName != 0)
            {
                string strLog = "Set GPIO failed " + ErrorsCatcher.GetReturnCodeDesc(ErrorsCatcher.errorName) + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
                return;
            }
            else
            {
                string strLog = "Set GPIO success" + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
            }
        }

        private void buttonGPIOGet_Click(object sender, EventArgs e)
        {
            byte OutputPin = 0;
            ErrorsCatcher.errorName = RWDev.GetGPIOStatus(ref RWDev.comAddress, ref OutputPin, RWDev.frmComPortIndex);
            if (ErrorsCatcher.errorName != 0)
            {
                string strLog = "Get GPIO status failed " + ErrorsCatcher.GetReturnCodeDesc(ErrorsCatcher.errorName) + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
                return;
            }
            else
            {
                if ((OutputPin & 0x10) == 0x10)
                    checkBoxOUT1.Checked = true;
                else
                    checkBoxOUT1.Checked = false;

                if ((OutputPin & 0x20) == 0x20)
                    checkBoxOUT2.Checked = true;
                else
                    checkBoxOUT2.Checked = false;

                if ((OutputPin & 0x01) == 1)
                    checkBoxINT1.Checked = true;
                else
                    checkBoxINT1.Checked = false;

                if ((OutputPin & 0x02) == 2)
                    checkBoxINT2.Checked = true;
                else
                    checkBoxINT2.Checked = false;

                string strLog = "Get GPIO status success " + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
            }
        }

        private void buttonReadSerial_Click(object sender, EventArgs e)
        {
            byte[] SeriaNo = new byte[4];
            textBoxReaderSerialNumber.Text = "";
            ErrorsCatcher.errorName = RWDev.GetSeriaNo(ref RWDev.comAddress, SeriaNo, RWDev.frmComPortIndex);
            if (ErrorsCatcher.errorName != 0)
            {
                string strLog = "Read serial number failed " + ErrorsCatcher.GetReturnCodeDesc(ErrorsCatcher.errorName) + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
                return;
            }
            else
            {
                string strLog = "Read serial number success" + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
            }
        }

        private void buttonPowerSet_Click(object sender, EventArgs e)
        {
            byte powerDbm = (byte)comboBoxPower.SelectedIndex;
            ErrorsCatcher.errorName = RWDev.SetRfPower(ref RWDev.comAddress, powerDbm, RWDev.frmComPortIndex);
            if (ErrorsCatcher.errorName != 0)
            {
                string strLog = "Set power failed " + ErrorsCatcher.GetReturnCodeDesc(ErrorsCatcher.errorName) + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
                return;
            }
            else
            {
                string strLog = "Set address success" + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
            }
        }

        private void buttonSetBaudRate_Click(object sender, EventArgs e)
        {
            byte fBaud = (byte)comboBoxBaud.SelectedIndex;
            if (fBaud > 2)
                fBaud = (byte)(fBaud + 2);
            ErrorsCatcher.errorName = RWDev.SetBaudRate(ref RWDev.comAddress, fBaud, RWDev.frmComPortIndex);
            if (ErrorsCatcher.errorName != 0)
            {
                string strLog = "Set baud rate failed " + ErrorsCatcher.GetReturnCodeDesc(ErrorsCatcher.errorName) + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
                return;
            }
            else
            {
                string strLog = "Set baud rate success" + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
            }
        }

        private void buttonSetRegion_Click(object sender, EventArgs e)
        {
            byte dminfre, dmaxfre;
            int band = 2;
            if (radioButtonChineeseBand2.Checked)
                band = 1;
            if (radioButtonUSBand.Checked)
                band = 2;
            if (radioButtonChineeseBand.Checked)
                band = 3;
            if (radioButtonEUband.Checked)
                band = 4;
            dminfre = Convert.ToByte(((band & 3) << 6) | (comboBoxDminfre.SelectedIndex & 0x3F));
            dmaxfre = Convert.ToByte(((band & 0x0c) << 4) | (comboBoxDmaxfre.SelectedIndex & 0x3F));
            ErrorsCatcher.errorName = RWDev.SetRegion(ref RWDev.comAddress, dmaxfre, dminfre, RWDev.frmComPortIndex);
            if (ErrorsCatcher.errorName != 0)
            {
                string strLog = "Set region failed " + ErrorsCatcher.GetReturnCodeDesc(ErrorsCatcher.errorName) + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
                return;
            }
            else
            {
                string strLog = "Set region success" + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
            }
        }

        private void buttonSetBuffLength_Click(object sender, EventArgs e)
        {
            byte SaveLen = 0;
            
            if (radioButton128bitBuff.Checked)
            {
                SaveLen = 0;
            }
            else
            {
                SaveLen = 1;
            }
            ErrorsCatcher.errorName = RWDev.SetSaveLen(ref RWDev.comAddress, SaveLen, RWDev.frmComPortIndex);
            if (ErrorsCatcher.errorName == 0)
            {
                string strLog = "Set buffer EPC/TID length success " + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
            }
            else
            {
                string strLog = "Set buffer EPC/TID length failed " + ErrorsCatcher.GetReturnCodeDesc(ErrorsCatcher.errorName) + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
                return;
            }
        }

        private void buttonBeepSet_Click(object sender, EventArgs e)
        {
            byte BeepEn = 0;            
            if (radioButtonBeepOpen.Checked)
                BeepEn = 1;
            else
                BeepEn = 0;
            ErrorsCatcher.errorName = RWDev.SetBeepNotification(ref RWDev.comAddress, BeepEn, RWDev.frmComPortIndex);
            if (ErrorsCatcher.errorName != 0)
            {
                string strLog = "Set beep failed " + ErrorsCatcher.GetReturnCodeDesc(ErrorsCatcher.errorName) + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
                return;
            }
            else
            {
                string strLog = "Set beep success" + Environment.NewLine;
                richTextBoxLogs.AppendText(strLog);
            }
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            richTextBoxLogs.Clear();
        }

        private void radioButtonUSBand_CheckedChanged(object sender, EventArgs e)
        {
            int i;
            comboBoxDmaxfre.Items.Clear();
            comboBoxDminfre.Items.Clear();
            for (i = 0; i < 50; i++)
            {
                comboBoxDminfre.Items.Add(Convert.ToString(902.75 + i * 0.5) + " MHz");
                comboBoxDmaxfre.Items.Add(Convert.ToString(902.75 + i * 0.5) + " MHz");
            }
            comboBoxDmaxfre.SelectedIndex = 49;
            comboBoxDminfre.SelectedIndex = 0;
        }

        private void radioButtonChineeseBand2_CheckedChanged(object sender, EventArgs e)
        {
            int i;
            comboBoxDmaxfre.Items.Clear();
            comboBoxDminfre.Items.Clear();
            for (i = 0; i < 20; i++)
            {
                comboBoxDminfre.Items.Add(Convert.ToString(920.125 + i * 0.25) + " MHz");
                comboBoxDmaxfre.Items.Add(Convert.ToString(920.125 + i * 0.25) + " MHz");
            }
            comboBoxDmaxfre.SelectedIndex = 19;
            comboBoxDminfre.SelectedIndex = 0;
        }

        private void radioButtonChineeseBand_CheckedChanged(object sender, EventArgs e)
        {
            int i;
            comboBoxDmaxfre.Items.Clear();
            comboBoxDminfre.Items.Clear();
            for (i = 0; i < 32; i++)
            {
                comboBoxDminfre.Items.Add(Convert.ToString(917.1 + i * 0.2) + " MHz");
                comboBoxDmaxfre.Items.Add(Convert.ToString(917.1 + i * 0.2) + " MHz");
            }
            comboBoxDmaxfre.SelectedIndex = 31;
            comboBoxDminfre.SelectedIndex = 0;
        }

        private void radioButtonEUband_CheckedChanged(object sender, EventArgs e)
        {
            int i;
            comboBoxDminfre.Items.Clear();
            comboBoxDmaxfre.Items.Clear();
            for (i = 0; i < 15; i++)
            {
                comboBoxDminfre.Items.Add(Convert.ToString(865.1 + i * 0.2) + " MHz");
                comboBoxDmaxfre.Items.Add(Convert.ToString(865.1 + i * 0.2) + " MHz");
            }
            comboBoxDmaxfre.SelectedIndex = 14;
            comboBoxDminfre.SelectedIndex = 0;
        }

        private void richTextBoxLogs_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelOperationRecords_Click(object sender, EventArgs e)
        {

        }
    }
}

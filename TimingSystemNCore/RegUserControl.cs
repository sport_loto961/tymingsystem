﻿using System;
using System.Collections.Generic;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SendGRPC;
using System.Drawing.Text;
using UHF;
using Converter;
using Errors;

namespace TimingSystemNCore
{
    public partial class RegUserControl : UserControl
    {
        private int userID;
        private int competitionID;
        private Dictionary<string, int> competitionsNameId = new Dictionary<string, int>();
        public RegUserControl()
        {
            InitializeComponent();
        }


        private void RegUserControl_Load(object sender, EventArgs e)
        {
            DisabledForm();
        }

        //Получение всех доступных соревнований
        private void buttonFindCompetitions_Click(object sender, EventArgs e)
        {
            //
            comboBoxCompetitions.Items.Clear();
            competitionsNameId.Clear();
            try
            {
                Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);
                var client = new Greeter.GreeterClient(channel);
                var reply = client.GetCompetitions(new CompetitionsRequest { IsGet = true });
                foreach(Competition c in reply.Competitions)
                {
                    richTextBoxLogs.AppendText(c.CompitionName + Environment.NewLine);
                    comboBoxCompetitions.Items.Add(c.CompitionName);
                    competitionsNameId.Add(c.CompitionName, c.CompetitionID);
                }
                comboBoxCompetitions.SelectedIndex = 0;
                richTextBoxLogs.AppendText("find competitions success!" + Environment.NewLine);
            }
            catch(Exception ex)
            {
                richTextBoxLogs.AppendText("Can't find competitions" + $"Exception: {ex.Message}" + Environment.NewLine);
            }
        }

        //Поиск участника с указанным в textBoxID стартовым номером 
        //и с указанным в comboBoxCompetitions номером соревнований.
        private void btnFindPerson_Click(object sender, EventArgs e)
        {
            userID = Convert.ToInt32(textBoxID.Text);
            competitionID = competitionsNameId[comboBoxCompetitions.SelectedItem.ToString()];

            try 
            {
                Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);
                var client = new Greeter.GreeterClient(channel);
                var reply = client.FindUserByID(new FindRequest { UserID = userID, CompetitionID = competitionID });
                if (reply.IsFind)
                {
                    richTextBoxLogs.AppendText(reply.FirstName + " " + reply.LastName + " is find." + Environment.NewLine);
                    EnabledForm();
                }
                if (!reply.IsFind)
                {
                    richTextBoxLogs.AppendText("Can't find sportsmen with ID: " + userID.ToString() + Environment.NewLine);
                    DisabledForm();
                }
                channel.ShutdownAsync().Wait();
            }
            catch (Exception ex)
            {
                richTextBoxLogs.AppendText("Send message failed!"+ $"Exception: {ex.Message}" + Environment.NewLine);
            }
                        
        }

        private void sendMessageToServer(string s)
        {
            MessageBox.Show(s);
        }

        private void EnabledForm()
        {
            groupBoxWriteEPC.Enabled = true;
        }

        private void DisabledForm()
        {
            groupBoxWriteEPC.Enabled = false;
        }

        private string getMessageFromServer()
        {
            string s = "message";
            return s;
        }

        private void buttonWrite_Click(object sender, EventArgs e)
        {
            byte Ant = 0;
            int CardNum = 0;
            int Totallen = 0;
            int EPClen, m;
            byte[] EPC = new byte[50000];
            int CardIndex;
            string temps, temp;
            //temp = "";
            string sEPC ="";
            byte MaskMem = 0;
            byte[] MaskAdr = new byte[2];
            byte MaskLen = 0;
            byte[] MaskData = new byte[100];
            byte MaskFlag = 0;
            byte AdrTID = 0;
            byte LenTID = 0;
            //int errorName;
            ErrorsCatcher.errorName = RWDev.Inventory_G2(ref RWDev.comAddress, RWDev.QValue, RWDev.session, MaskMem, MaskAdr,
                                            MaskLen, MaskData, MaskFlag, AdrTID, LenTID, RWDev.TIDFlag, RWDev.target,
                                            RWDev.inAnt, RWDev.scantime, RWDev.fastFlag, EPC, ref Ant, ref Totallen,
                                            ref CardNum, RWDev.frmComPortIndex);
            byte[] daw = new byte[Totallen];
            Array.Copy(EPC, daw, Totallen);
            temps = ConverterTo.ByteArrayToHexString(daw);
            
            m = 0;
            if (CardNum == 0) return;

            for (CardIndex = 0; CardIndex < CardNum; CardIndex++)
            {
                EPClen = daw[m] + 1;
                temp = temps.Substring(m * 2 + 2, EPClen * 2);
                sEPC = temp.Substring(0, temp.Length - 2);
            }

            try
            {
                Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);
                var client = new Greeter.GreeterClient(channel);

                var reply = client.WriteRFID(new WriteRequest { CompetitionID = competitionID ,RFID = sEPC, UserID = userID });
                if (reply.IsWrite)
                {
                    richTextBoxLogs.AppendText("Writed has successfully" + Environment.NewLine);
                    DisabledForm();
                }
                if (!reply.IsWrite)
                {
                    richTextBoxLogs.AppendText("Write error, please try again" + Environment.NewLine);
                    EnabledForm();
                }
                channel.ShutdownAsync().Wait();
            }
            catch(Exception ex)
            {
                richTextBoxLogs.AppendText("Write error!" + $"Exception: {ex.Message}" + Environment.NewLine);
            }


        }

        private void richTextBoxLogs_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            richTextBoxLogs.Clear();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}

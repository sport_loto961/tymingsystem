﻿namespace TimingSystemNCore
{
    partial class RaceUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnStartRace = new System.Windows.Forms.Button();
            this.buttonSendRaceData = new System.Windows.Forms.Button();
            this.ScanTimer = new System.Windows.Forms.Timer(this.components);
            this.commboBoxScanTime = new System.Windows.Forms.ComboBox();
            this.labelScanTime = new System.Windows.Forms.Label();
            this.btnGetRaceData = new System.Windows.Forms.Button();
            this.richTextBoxLogs = new System.Windows.Forms.RichTextBox();
            this.labelOperationRecords = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EPC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lap = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonFindCompetitions = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxCompetitions = new System.Windows.Forms.ComboBox();
            this.labelCompetitions = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStartRace
            // 
            this.btnStartRace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.btnStartRace.FlatAppearance.BorderSize = 0;
            this.btnStartRace.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartRace.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnStartRace.ForeColor = System.Drawing.SystemColors.Control;
            this.btnStartRace.Location = new System.Drawing.Point(441, 286);
            this.btnStartRace.Name = "btnStartRace";
            this.btnStartRace.Size = new System.Drawing.Size(220, 38);
            this.btnStartRace.TabIndex = 1;
            this.btnStartRace.Text = "Start";
            this.btnStartRace.UseVisualStyleBackColor = false;
            this.btnStartRace.Click += new System.EventHandler(this.btnStartRace_Click);
            // 
            // buttonSendRaceData
            // 
            this.buttonSendRaceData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonSendRaceData.FlatAppearance.BorderSize = 0;
            this.buttonSendRaceData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSendRaceData.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonSendRaceData.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonSendRaceData.Location = new System.Drawing.Point(438, 345);
            this.buttonSendRaceData.Name = "buttonSendRaceData";
            this.buttonSendRaceData.Size = new System.Drawing.Size(220, 38);
            this.buttonSendRaceData.TabIndex = 1;
            this.buttonSendRaceData.Text = "Send race data";
            this.buttonSendRaceData.UseVisualStyleBackColor = false;
            this.buttonSendRaceData.Click += new System.EventHandler(this.buttonSendRaceData_Click);
            // 
            // ScanTimer
            // 
            this.ScanTimer.Tick += new System.EventHandler(this.ScanTimer_Tick);
            // 
            // commboBoxScanTime
            // 
            this.commboBoxScanTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.commboBoxScanTime.FormattingEnabled = true;
            this.commboBoxScanTime.Location = new System.Drawing.Point(568, 239);
            this.commboBoxScanTime.Name = "commboBoxScanTime";
            this.commboBoxScanTime.Size = new System.Drawing.Size(93, 23);
            this.commboBoxScanTime.TabIndex = 2;
            this.commboBoxScanTime.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // labelScanTime
            // 
            this.labelScanTime.AutoSize = true;
            this.labelScanTime.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelScanTime.Location = new System.Drawing.Point(441, 242);
            this.labelScanTime.Name = "labelScanTime";
            this.labelScanTime.Size = new System.Drawing.Size(121, 20);
            this.labelScanTime.TabIndex = 3;
            this.labelScanTime.Text = "Max-Scan Time:";
            this.labelScanTime.Click += new System.EventHandler(this.labelScanTime_Click);
            // 
            // btnGetRaceData
            // 
            this.btnGetRaceData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.btnGetRaceData.FlatAppearance.BorderSize = 0;
            this.btnGetRaceData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetRaceData.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnGetRaceData.ForeColor = System.Drawing.SystemColors.Control;
            this.btnGetRaceData.Location = new System.Drawing.Point(441, 183);
            this.btnGetRaceData.Name = "btnGetRaceData";
            this.btnGetRaceData.Size = new System.Drawing.Size(220, 38);
            this.btnGetRaceData.TabIndex = 1;
            this.btnGetRaceData.Text = "Get race data";
            this.btnGetRaceData.UseVisualStyleBackColor = false;
            this.btnGetRaceData.Click += new System.EventHandler(this.btnGetRaceData_Click);
            // 
            // richTextBoxLogs
            // 
            this.richTextBoxLogs.Location = new System.Drawing.Point(12, 428);
            this.richTextBoxLogs.Name = "richTextBoxLogs";
            this.richTextBoxLogs.Size = new System.Drawing.Size(658, 96);
            this.richTextBoxLogs.TabIndex = 5;
            this.richTextBoxLogs.Text = "";
            // 
            // labelOperationRecords
            // 
            this.labelOperationRecords.AutoSize = true;
            this.labelOperationRecords.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelOperationRecords.Location = new System.Drawing.Point(31, 408);
            this.labelOperationRecords.Name = "labelOperationRecords";
            this.labelOperationRecords.Size = new System.Drawing.Size(129, 17);
            this.labelOperationRecords.TabIndex = 3;
            this.labelOperationRecords.Text = "Operation records:";
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.Control;
            this.btnRefresh.Location = new System.Drawing.Point(562, 402);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(96, 23);
            this.btnRefresh.TabIndex = 1;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // Num
            // 
            this.Num.HeaderText = "Num";
            this.Num.Name = "Num";
            this.Num.ReadOnly = true;
            this.Num.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Num.Width = 40;
            // 
            // EPC
            // 
            this.EPC.HeaderText = "EPC";
            this.EPC.Name = "EPC";
            this.EPC.ReadOnly = true;
            this.EPC.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EPC.Width = 170;
            // 
            // Time
            // 
            this.Time.HeaderText = "Time";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Lap
            // 
            this.Lap.HeaderText = "Lap";
            this.Lap.Name = "Lap";
            this.Lap.ReadOnly = true;
            this.Lap.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Num,
            this.EPC,
            this.Time,
            this.Lap});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(11, 10);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(413, 373);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.Text = "dataGridView1";
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // buttonFindCompetitions
            // 
            this.buttonFindCompetitions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonFindCompetitions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFindCompetitions.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonFindCompetitions.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonFindCompetitions.Location = new System.Drawing.Point(17, 31);
            this.buttonFindCompetitions.Name = "buttonFindCompetitions";
            this.buttonFindCompetitions.Size = new System.Drawing.Size(189, 38);
            this.buttonFindCompetitions.TabIndex = 1;
            this.buttonFindCompetitions.Text = "Find";
            this.buttonFindCompetitions.UseVisualStyleBackColor = false;
            this.buttonFindCompetitions.Click += new System.EventHandler(this.buttonFindCompetitions_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxCompetitions);
            this.groupBox1.Controls.Add(this.labelCompetitions);
            this.groupBox1.Controls.Add(this.buttonFindCompetitions);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBox1.Location = new System.Drawing.Point(441, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(220, 155);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Find competitions";
            // 
            // comboBoxCompetitions
            // 
            this.comboBoxCompetitions.FormattingEnabled = true;
            this.comboBoxCompetitions.Location = new System.Drawing.Point(17, 113);
            this.comboBoxCompetitions.Name = "comboBoxCompetitions";
            this.comboBoxCompetitions.Size = new System.Drawing.Size(189, 24);
            this.comboBoxCompetitions.TabIndex = 3;
            // 
            // labelCompetitions
            // 
            this.labelCompetitions.AutoSize = true;
            this.labelCompetitions.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelCompetitions.Location = new System.Drawing.Point(19, 89);
            this.labelCompetitions.Name = "labelCompetitions";
            this.labelCompetitions.Size = new System.Drawing.Size(119, 21);
            this.labelCompetitions.TabIndex = 2;
            this.labelCompetitions.Text = "Competitions:";
            // 
            // RaceUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.labelOperationRecords);
            this.Controls.Add(this.richTextBoxLogs);
            this.Controls.Add(this.labelScanTime);
            this.Controls.Add(this.commboBoxScanTime);
            this.Controls.Add(this.buttonSendRaceData);
            this.Controls.Add(this.btnGetRaceData);
            this.Controls.Add(this.btnStartRace);
            this.Controls.Add(this.dataGridView1);
            this.Name = "RaceUserControl";
            this.Size = new System.Drawing.Size(680, 536);
            this.Load += new System.EventHandler(this.RaceUserControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnStartRace;
        private System.Windows.Forms.Button buttonSendRaceData;
        private System.Windows.Forms.Timer ScanTimer;
        private System.Windows.Forms.ComboBox commboBoxScanTime;
        private System.Windows.Forms.Label labelScanTime;
        private System.Windows.Forms.Button btnGetRaceData;
        private System.Windows.Forms.RichTextBox richTextBoxLogs;
        private System.Windows.Forms.Label labelOperationRecords;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Num;
        private System.Windows.Forms.DataGridViewTextBoxColumn EPC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lap;
        private System.Windows.Forms.Button buttonFindCompetitions;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxCompetitions;
        private System.Windows.Forms.Label labelCompetitions;
    }
}

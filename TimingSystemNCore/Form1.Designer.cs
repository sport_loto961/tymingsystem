﻿namespace TimingSystemNCore
{
    
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;        

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRace = new System.Windows.Forms.Button();
            this.btnReg = new System.Windows.Forms.Button();
            this.btnInit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.regUserControl = new TimingSystemNCore.RegUserControl();
            this.raceUserControl = new TimingSystemNCore.RaceUserControl();
            this.initUserControl = new TimingSystemNCore.InitUserControl();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRace
            // 
            this.btnRace.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRace.FlatAppearance.BorderSize = 0;
            this.btnRace.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRace.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnRace.ForeColor = System.Drawing.SystemColors.Control;
            this.btnRace.Location = new System.Drawing.Point(0, 170);
            this.btnRace.Name = "btnRace";
            this.btnRace.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.btnRace.Size = new System.Drawing.Size(200, 35);
            this.btnRace.TabIndex = 1;
            this.btnRace.Text = "Race";
            this.btnRace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRace.UseVisualStyleBackColor = true;
            this.btnRace.Click += new System.EventHandler(this.btnRace_Click);
            // 
            // btnReg
            // 
            this.btnReg.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnReg.FlatAppearance.BorderSize = 0;
            this.btnReg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReg.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnReg.ForeColor = System.Drawing.SystemColors.Control;
            this.btnReg.Location = new System.Drawing.Point(0, 135);
            this.btnReg.Name = "btnReg";
            this.btnReg.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.btnReg.Size = new System.Drawing.Size(200, 35);
            this.btnReg.TabIndex = 1;
            this.btnReg.Text = "Registration";
            this.btnReg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReg.UseVisualStyleBackColor = true;
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            // 
            // btnInit
            // 
            this.btnInit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnInit.FlatAppearance.BorderSize = 0;
            this.btnInit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInit.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnInit.ForeColor = System.Drawing.SystemColors.Control;
            this.btnInit.Location = new System.Drawing.Point(0, 100);
            this.btnInit.Name = "btnInit";
            this.btnInit.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.btnInit.Size = new System.Drawing.Size(200, 35);
            this.btnInit.TabIndex = 1;
            this.btnInit.Text = "Initialization";
            this.btnInit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInit.UseVisualStyleBackColor = true;
            this.btnInit.Click += new System.EventHandler(this.btnInit_Click);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.panel2.Controls.Add(this.btnRace);
            this.panel2.Controls.Add(this.btnReg);
            this.panel2.Controls.Add(this.btnInit);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 539);
            this.panel2.TabIndex = 0;
            // 
            // regUserControl
            // 
            this.regUserControl.Location = new System.Drawing.Point(200, 0);
            this.regUserControl.Name = "regUserControl";
            this.regUserControl.Size = new System.Drawing.Size(686, 539);
            this.regUserControl.TabIndex = 1;
            this.regUserControl.Load += new System.EventHandler(this.regUserControl_Load_1);
            // 
            // raceUserControl
            // 
            this.raceUserControl.BackColor = System.Drawing.SystemColors.Control;
            this.raceUserControl.Location = new System.Drawing.Point(200, 0);
            this.raceUserControl.Name = "raceUserControl";
            this.raceUserControl.Size = new System.Drawing.Size(686, 539);
            this.raceUserControl.TabIndex = 2;
            this.raceUserControl.Load += new System.EventHandler(this.userControl_Handler);
            // 
            // initUserControl
            // 
            this.initUserControl.Location = new System.Drawing.Point(200, 0);
            this.initUserControl.Name = "initUserControl";
            this.initUserControl.Size = new System.Drawing.Size(699, 544);
            this.initUserControl.TabIndex = 3;
            this.initUserControl.Load += new System.EventHandler(this.initUserControl_Load);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 539);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.raceUserControl);
            this.Controls.Add(this.initUserControl);
            this.Controls.Add(this.regUserControl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRace;
        private System.Windows.Forms.Button btnReg;
        private System.Windows.Forms.Button btnInit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private RegUserControl regUserControl;
        private RaceUserControl raceUserControl;
        private InitUserControl initUserControl;
    }
}


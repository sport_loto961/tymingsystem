﻿namespace TimingSystemNCore
{
    partial class InitUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnectReader = new System.Windows.Forms.Button();
            this.btnDisconnectReader = new System.Windows.Forms.Button();
            this.comboBoxCOM = new System.Windows.Forms.ComboBox();
            this.comboBoxBaud = new System.Windows.Forms.ComboBox();
            this.labelPort = new System.Windows.Forms.Label();
            this.labelBaud = new System.Windows.Forms.Label();
            this.groupBoxRS232 = new System.Windows.Forms.GroupBox();
            this.btnSetAdr = new System.Windows.Forms.Button();
            this.groupBoxReaderAddress = new System.Windows.Forms.GroupBox();
            this.textBoxReadAddress = new System.Windows.Forms.TextBox();
            this.textBoxReaderSerialNumber = new System.Windows.Forms.TextBox();
            this.buttonReadSerial = new System.Windows.Forms.Button();
            this.groupBoxRedaerSerialNumber = new System.Windows.Forms.GroupBox();
            this.buttonGPIOSet = new System.Windows.Forms.Button();
            this.groupBoxGPIOOperation = new System.Windows.Forms.GroupBox();
            this.checkBoxINT2 = new System.Windows.Forms.CheckBox();
            this.checkBoxINT1 = new System.Windows.Forms.CheckBox();
            this.checkBoxOUT2 = new System.Windows.Forms.CheckBox();
            this.checkBoxOUT1 = new System.Windows.Forms.CheckBox();
            this.buttonGPIOGet = new System.Windows.Forms.Button();
            this.textBoxFirmwareVersion = new System.Windows.Forms.TextBox();
            this.groupBoxFirmwareVersion = new System.Windows.Forms.GroupBox();
            this.buttonBeepSet = new System.Windows.Forms.Button();
            this.groupBoxBeep = new System.Windows.Forms.GroupBox();
            this.radioButtonBeepClose = new System.Windows.Forms.RadioButton();
            this.radioButtonBeepOpen = new System.Windows.Forms.RadioButton();
            this.buttonPowerSet = new System.Windows.Forms.Button();
            this.comboBoxPower = new System.Windows.Forms.ComboBox();
            this.groupBoxPower = new System.Windows.Forms.GroupBox();
            this.labeldBm = new System.Windows.Forms.Label();
            this.buttonSetBaudRate = new System.Windows.Forms.Button();
            this.comboBoxSetBaudRate = new System.Windows.Forms.ComboBox();
            this.groupBoxDBM = new System.Windows.Forms.GroupBox();
            this.buttonSetRegion = new System.Windows.Forms.Button();
            this.labelMinfre = new System.Windows.Forms.Label();
            this.comboBoxDminfre = new System.Windows.Forms.ComboBox();
            this.comboBoxDmaxfre = new System.Windows.Forms.ComboBox();
            this.groupBoxRegion = new System.Windows.Forms.GroupBox();
            this.radioButtonEUband = new System.Windows.Forms.RadioButton();
            this.radioButtonChineeseBand = new System.Windows.Forms.RadioButton();
            this.radioButtonChineeseBand2 = new System.Windows.Forms.RadioButton();
            this.radioButtonUSBand = new System.Windows.Forms.RadioButton();
            this.checkBoxRegionSingle = new System.Windows.Forms.CheckBox();
            this.labelMaxfre = new System.Windows.Forms.Label();
            this.groupBoxBuffLength = new System.Windows.Forms.GroupBox();
            this.buttonGetBuffLength = new System.Windows.Forms.Button();
            this.radioButton496bitBuff = new System.Windows.Forms.RadioButton();
            this.radioButton128bitBuff = new System.Windows.Forms.RadioButton();
            this.buttonSetBuffLength = new System.Windows.Forms.Button();
            this.richTextBoxLogs = new System.Windows.Forms.RichTextBox();
            this.labelOperationRecords = new System.Windows.Forms.Label();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.groupBoxRS232.SuspendLayout();
            this.groupBoxReaderAddress.SuspendLayout();
            this.groupBoxRedaerSerialNumber.SuspendLayout();
            this.groupBoxGPIOOperation.SuspendLayout();
            this.groupBoxFirmwareVersion.SuspendLayout();
            this.groupBoxBeep.SuspendLayout();
            this.groupBoxPower.SuspendLayout();
            this.groupBoxDBM.SuspendLayout();
            this.groupBoxRegion.SuspendLayout();
            this.groupBoxBuffLength.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConnectReader
            // 
            this.btnConnectReader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.btnConnectReader.FlatAppearance.BorderSize = 0;
            this.btnConnectReader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnectReader.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnConnectReader.ForeColor = System.Drawing.SystemColors.Control;
            this.btnConnectReader.Location = new System.Drawing.Point(216, 21);
            this.btnConnectReader.Name = "btnConnectReader";
            this.btnConnectReader.Size = new System.Drawing.Size(96, 23);
            this.btnConnectReader.TabIndex = 1;
            this.btnConnectReader.Text = "Connect";
            this.btnConnectReader.UseVisualStyleBackColor = false;
            this.btnConnectReader.Click += new System.EventHandler(this.btnConnectReader_Click);
            // 
            // btnDisconnectReader
            // 
            this.btnDisconnectReader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.btnDisconnectReader.Enabled = false;
            this.btnDisconnectReader.FlatAppearance.BorderSize = 0;
            this.btnDisconnectReader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDisconnectReader.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnDisconnectReader.ForeColor = System.Drawing.SystemColors.Control;
            this.btnDisconnectReader.Location = new System.Drawing.Point(216, 54);
            this.btnDisconnectReader.Name = "btnDisconnectReader";
            this.btnDisconnectReader.Size = new System.Drawing.Size(96, 23);
            this.btnDisconnectReader.TabIndex = 1;
            this.btnDisconnectReader.Text = "Disconnect";
            this.btnDisconnectReader.UseVisualStyleBackColor = false;
            this.btnDisconnectReader.Click += new System.EventHandler(this.btnDisconnectReader_Click);
            // 
            // comboBoxCOM
            // 
            this.comboBoxCOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCOM.FormattingEnabled = true;
            this.comboBoxCOM.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9"});
            this.comboBoxCOM.Location = new System.Drawing.Point(89, 21);
            this.comboBoxCOM.Name = "comboBoxCOM";
            this.comboBoxCOM.Size = new System.Drawing.Size(121, 24);
            this.comboBoxCOM.TabIndex = 2;
            // 
            // comboBoxBaud
            // 
            this.comboBoxBaud.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBaud.FormattingEnabled = true;
            this.comboBoxBaud.Items.AddRange(new object[] {
            "9600bps",
            "19200bps",
            "38400bps",
            "57600bps",
            "115200bps"});
            this.comboBoxBaud.Location = new System.Drawing.Point(89, 54);
            this.comboBoxBaud.Name = "comboBoxBaud";
            this.comboBoxBaud.Size = new System.Drawing.Size(121, 24);
            this.comboBoxBaud.TabIndex = 2;
            // 
            // labelPort
            // 
            this.labelPort.AutoSize = true;
            this.labelPort.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelPort.Location = new System.Drawing.Point(15, 24);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(38, 17);
            this.labelPort.TabIndex = 3;
            this.labelPort.Text = "Port:";
            // 
            // labelBaud
            // 
            this.labelBaud.AutoSize = true;
            this.labelBaud.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelBaud.Location = new System.Drawing.Point(12, 57);
            this.labelBaud.Name = "labelBaud";
            this.labelBaud.Size = new System.Drawing.Size(75, 17);
            this.labelBaud.TabIndex = 3;
            this.labelBaud.Text = "Baud rate:";
            // 
            // groupBoxRS232
            // 
            this.groupBoxRS232.Controls.Add(this.btnConnectReader);
            this.groupBoxRS232.Controls.Add(this.labelBaud);
            this.groupBoxRS232.Controls.Add(this.btnDisconnectReader);
            this.groupBoxRS232.Controls.Add(this.labelPort);
            this.groupBoxRS232.Controls.Add(this.comboBoxCOM);
            this.groupBoxRS232.Controls.Add(this.comboBoxBaud);
            this.groupBoxRS232.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxRS232.Location = new System.Drawing.Point(12, 6);
            this.groupBoxRS232.Name = "groupBoxRS232";
            this.groupBoxRS232.Size = new System.Drawing.Size(324, 87);
            this.groupBoxRS232.TabIndex = 4;
            this.groupBoxRS232.TabStop = false;
            this.groupBoxRS232.Text = "RS232";
            // 
            // btnSetAdr
            // 
            this.btnSetAdr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.btnSetAdr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetAdr.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSetAdr.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSetAdr.Location = new System.Drawing.Point(216, 26);
            this.btnSetAdr.Name = "btnSetAdr";
            this.btnSetAdr.Size = new System.Drawing.Size(96, 23);
            this.btnSetAdr.TabIndex = 1;
            this.btnSetAdr.Text = "Set";
            this.btnSetAdr.UseVisualStyleBackColor = false;
            this.btnSetAdr.Click += new System.EventHandler(this.btnSetAdr_Click);
            // 
            // groupBoxReaderAddress
            // 
            this.groupBoxReaderAddress.Controls.Add(this.textBoxReadAddress);
            this.groupBoxReaderAddress.Controls.Add(this.btnSetAdr);
            this.groupBoxReaderAddress.Enabled = false;
            this.groupBoxReaderAddress.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxReaderAddress.Location = new System.Drawing.Point(12, 94);
            this.groupBoxReaderAddress.Name = "groupBoxReaderAddress";
            this.groupBoxReaderAddress.Size = new System.Drawing.Size(324, 60);
            this.groupBoxReaderAddress.TabIndex = 4;
            this.groupBoxReaderAddress.TabStop = false;
            this.groupBoxReaderAddress.Text = "Reader address";
            // 
            // textBoxReadAddress
            // 
            this.textBoxReadAddress.Location = new System.Drawing.Point(89, 26);
            this.textBoxReadAddress.Name = "textBoxReadAddress";
            this.textBoxReadAddress.Size = new System.Drawing.Size(121, 23);
            this.textBoxReadAddress.TabIndex = 2;
            this.textBoxReadAddress.Text = "00";
            // 
            // textBoxReaderSerialNumber
            // 
            this.textBoxReaderSerialNumber.Location = new System.Drawing.Point(89, 26);
            this.textBoxReaderSerialNumber.Name = "textBoxReaderSerialNumber";
            this.textBoxReaderSerialNumber.Size = new System.Drawing.Size(121, 23);
            this.textBoxReaderSerialNumber.TabIndex = 2;
            // 
            // buttonReadSerial
            // 
            this.buttonReadSerial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonReadSerial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReadSerial.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonReadSerial.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonReadSerial.Location = new System.Drawing.Point(216, 26);
            this.buttonReadSerial.Name = "buttonReadSerial";
            this.buttonReadSerial.Size = new System.Drawing.Size(96, 23);
            this.buttonReadSerial.TabIndex = 1;
            this.buttonReadSerial.Text = "Read";
            this.buttonReadSerial.UseVisualStyleBackColor = false;
            this.buttonReadSerial.Click += new System.EventHandler(this.buttonReadSerial_Click);
            // 
            // groupBoxRedaerSerialNumber
            // 
            this.groupBoxRedaerSerialNumber.Controls.Add(this.textBoxReaderSerialNumber);
            this.groupBoxRedaerSerialNumber.Controls.Add(this.buttonReadSerial);
            this.groupBoxRedaerSerialNumber.Enabled = false;
            this.groupBoxRedaerSerialNumber.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxRedaerSerialNumber.Location = new System.Drawing.Point(346, 94);
            this.groupBoxRedaerSerialNumber.Name = "groupBoxRedaerSerialNumber";
            this.groupBoxRedaerSerialNumber.Size = new System.Drawing.Size(324, 60);
            this.groupBoxRedaerSerialNumber.TabIndex = 4;
            this.groupBoxRedaerSerialNumber.TabStop = false;
            this.groupBoxRedaerSerialNumber.Text = "Reader serial number";
            // 
            // buttonGPIOSet
            // 
            this.buttonGPIOSet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonGPIOSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGPIOSet.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonGPIOSet.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonGPIOSet.Location = new System.Drawing.Point(216, 20);
            this.buttonGPIOSet.Name = "buttonGPIOSet";
            this.buttonGPIOSet.Size = new System.Drawing.Size(96, 23);
            this.buttonGPIOSet.TabIndex = 1;
            this.buttonGPIOSet.Text = "Set";
            this.buttonGPIOSet.UseVisualStyleBackColor = false;
            this.buttonGPIOSet.Click += new System.EventHandler(this.buttonGPIOSet_Click);
            // 
            // groupBoxGPIOOperation
            // 
            this.groupBoxGPIOOperation.Controls.Add(this.checkBoxINT2);
            this.groupBoxGPIOOperation.Controls.Add(this.checkBoxINT1);
            this.groupBoxGPIOOperation.Controls.Add(this.checkBoxOUT2);
            this.groupBoxGPIOOperation.Controls.Add(this.checkBoxOUT1);
            this.groupBoxGPIOOperation.Controls.Add(this.buttonGPIOGet);
            this.groupBoxGPIOOperation.Controls.Add(this.buttonGPIOSet);
            this.groupBoxGPIOOperation.Enabled = false;
            this.groupBoxGPIOOperation.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxGPIOOperation.Location = new System.Drawing.Point(346, 6);
            this.groupBoxGPIOOperation.Name = "groupBoxGPIOOperation";
            this.groupBoxGPIOOperation.Size = new System.Drawing.Size(324, 87);
            this.groupBoxGPIOOperation.TabIndex = 4;
            this.groupBoxGPIOOperation.TabStop = false;
            this.groupBoxGPIOOperation.Text = "GPIO Operation";
            // 
            // checkBoxINT2
            // 
            this.checkBoxINT2.AutoSize = true;
            this.checkBoxINT2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.checkBoxINT2.Location = new System.Drawing.Point(152, 51);
            this.checkBoxINT2.Name = "checkBoxINT2";
            this.checkBoxINT2.Size = new System.Drawing.Size(52, 21);
            this.checkBoxINT2.TabIndex = 2;
            this.checkBoxINT2.Text = "INT2";
            this.checkBoxINT2.UseVisualStyleBackColor = true;
            // 
            // checkBoxINT1
            // 
            this.checkBoxINT1.AutoSize = true;
            this.checkBoxINT1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.checkBoxINT1.Location = new System.Drawing.Point(89, 51);
            this.checkBoxINT1.Name = "checkBoxINT1";
            this.checkBoxINT1.Size = new System.Drawing.Size(52, 21);
            this.checkBoxINT1.TabIndex = 2;
            this.checkBoxINT1.Text = "INT1";
            this.checkBoxINT1.UseVisualStyleBackColor = true;
            // 
            // checkBoxOUT2
            // 
            this.checkBoxOUT2.AutoSize = true;
            this.checkBoxOUT2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.checkBoxOUT2.Location = new System.Drawing.Point(152, 24);
            this.checkBoxOUT2.Name = "checkBoxOUT2";
            this.checkBoxOUT2.Size = new System.Drawing.Size(58, 21);
            this.checkBoxOUT2.TabIndex = 2;
            this.checkBoxOUT2.Text = "OUT2";
            this.checkBoxOUT2.UseVisualStyleBackColor = true;
            // 
            // checkBoxOUT1
            // 
            this.checkBoxOUT1.AutoSize = true;
            this.checkBoxOUT1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.checkBoxOUT1.Location = new System.Drawing.Point(88, 24);
            this.checkBoxOUT1.Name = "checkBoxOUT1";
            this.checkBoxOUT1.Size = new System.Drawing.Size(58, 21);
            this.checkBoxOUT1.TabIndex = 2;
            this.checkBoxOUT1.Text = "OUT1";
            this.checkBoxOUT1.UseVisualStyleBackColor = true;
            // 
            // buttonGPIOGet
            // 
            this.buttonGPIOGet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonGPIOGet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGPIOGet.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonGPIOGet.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonGPIOGet.Location = new System.Drawing.Point(216, 49);
            this.buttonGPIOGet.Name = "buttonGPIOGet";
            this.buttonGPIOGet.Size = new System.Drawing.Size(96, 23);
            this.buttonGPIOGet.TabIndex = 1;
            this.buttonGPIOGet.Text = "Get";
            this.buttonGPIOGet.UseVisualStyleBackColor = false;
            this.buttonGPIOGet.Click += new System.EventHandler(this.buttonGPIOGet_Click);
            // 
            // textBoxFirmwareVersion
            // 
            this.textBoxFirmwareVersion.Location = new System.Drawing.Point(16, 35);
            this.textBoxFirmwareVersion.Name = "textBoxFirmwareVersion";
            this.textBoxFirmwareVersion.Size = new System.Drawing.Size(205, 23);
            this.textBoxFirmwareVersion.TabIndex = 2;
            // 
            // groupBoxFirmwareVersion
            // 
            this.groupBoxFirmwareVersion.Controls.Add(this.textBoxFirmwareVersion);
            this.groupBoxFirmwareVersion.Enabled = false;
            this.groupBoxFirmwareVersion.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxFirmwareVersion.Location = new System.Drawing.Point(435, 304);
            this.groupBoxFirmwareVersion.Name = "groupBoxFirmwareVersion";
            this.groupBoxFirmwareVersion.Size = new System.Drawing.Size(235, 86);
            this.groupBoxFirmwareVersion.TabIndex = 4;
            this.groupBoxFirmwareVersion.TabStop = false;
            this.groupBoxFirmwareVersion.Text = "Firmware version";
            // 
            // buttonBeepSet
            // 
            this.buttonBeepSet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonBeepSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBeepSet.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonBeepSet.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonBeepSet.Location = new System.Drawing.Point(87, 24);
            this.buttonBeepSet.Name = "buttonBeepSet";
            this.buttonBeepSet.Size = new System.Drawing.Size(96, 23);
            this.buttonBeepSet.TabIndex = 1;
            this.buttonBeepSet.Text = "Set";
            this.buttonBeepSet.UseVisualStyleBackColor = false;
            this.buttonBeepSet.Click += new System.EventHandler(this.buttonBeepSet_Click);
            // 
            // groupBoxBeep
            // 
            this.groupBoxBeep.Controls.Add(this.radioButtonBeepClose);
            this.groupBoxBeep.Controls.Add(this.radioButtonBeepOpen);
            this.groupBoxBeep.Controls.Add(this.buttonBeepSet);
            this.groupBoxBeep.Enabled = false;
            this.groupBoxBeep.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxBeep.Location = new System.Drawing.Point(228, 304);
            this.groupBoxBeep.Name = "groupBoxBeep";
            this.groupBoxBeep.Size = new System.Drawing.Size(194, 86);
            this.groupBoxBeep.TabIndex = 4;
            this.groupBoxBeep.TabStop = false;
            this.groupBoxBeep.Text = "Beep";
            // 
            // radioButtonBeepClose
            // 
            this.radioButtonBeepClose.AutoSize = true;
            this.radioButtonBeepClose.Checked = true;
            this.radioButtonBeepClose.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButtonBeepClose.Location = new System.Drawing.Point(19, 50);
            this.radioButtonBeepClose.Name = "radioButtonBeepClose";
            this.radioButtonBeepClose.Size = new System.Drawing.Size(62, 21);
            this.radioButtonBeepClose.TabIndex = 2;
            this.radioButtonBeepClose.TabStop = true;
            this.radioButtonBeepClose.Text = "Close";
            this.radioButtonBeepClose.UseVisualStyleBackColor = true;
            // 
            // radioButtonBeepOpen
            // 
            this.radioButtonBeepOpen.AutoSize = true;
            this.radioButtonBeepOpen.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButtonBeepOpen.Location = new System.Drawing.Point(19, 24);
            this.radioButtonBeepOpen.Name = "radioButtonBeepOpen";
            this.radioButtonBeepOpen.Size = new System.Drawing.Size(62, 21);
            this.radioButtonBeepOpen.TabIndex = 2;
            this.radioButtonBeepOpen.Text = "Open";
            this.radioButtonBeepOpen.UseVisualStyleBackColor = true;
            // 
            // buttonPowerSet
            // 
            this.buttonPowerSet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonPowerSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPowerSet.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonPowerSet.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonPowerSet.Location = new System.Drawing.Point(216, 22);
            this.buttonPowerSet.Name = "buttonPowerSet";
            this.buttonPowerSet.Size = new System.Drawing.Size(96, 23);
            this.buttonPowerSet.TabIndex = 1;
            this.buttonPowerSet.Text = "Set";
            this.buttonPowerSet.UseVisualStyleBackColor = false;
            this.buttonPowerSet.Click += new System.EventHandler(this.buttonPowerSet_Click);
            // 
            // comboBoxPower
            // 
            this.comboBoxPower.FormattingEnabled = true;
            this.comboBoxPower.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26"});
            this.comboBoxPower.Location = new System.Drawing.Point(46, 25);
            this.comboBoxPower.Name = "comboBoxPower";
            this.comboBoxPower.Size = new System.Drawing.Size(121, 24);
            this.comboBoxPower.TabIndex = 2;
            // 
            // groupBoxPower
            // 
            this.groupBoxPower.Controls.Add(this.labeldBm);
            this.groupBoxPower.Controls.Add(this.buttonPowerSet);
            this.groupBoxPower.Controls.Add(this.comboBoxPower);
            this.groupBoxPower.Enabled = false;
            this.groupBoxPower.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxPower.Location = new System.Drawing.Point(12, 155);
            this.groupBoxPower.Name = "groupBoxPower";
            this.groupBoxPower.Size = new System.Drawing.Size(324, 60);
            this.groupBoxPower.TabIndex = 4;
            this.groupBoxPower.TabStop = false;
            this.groupBoxPower.Text = "Power";
            // 
            // labeldBm
            // 
            this.labeldBm.AutoSize = true;
            this.labeldBm.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labeldBm.Location = new System.Drawing.Point(173, 28);
            this.labeldBm.Name = "labeldBm";
            this.labeldBm.Size = new System.Drawing.Size(37, 17);
            this.labeldBm.TabIndex = 3;
            this.labeldBm.Text = "dBm";
            this.labeldBm.Click += new System.EventHandler(this.labeldBm_Click);
            // 
            // buttonSetBaudRate
            // 
            this.buttonSetBaudRate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonSetBaudRate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetBaudRate.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonSetBaudRate.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonSetBaudRate.Location = new System.Drawing.Point(216, 22);
            this.buttonSetBaudRate.Name = "buttonSetBaudRate";
            this.buttonSetBaudRate.Size = new System.Drawing.Size(96, 23);
            this.buttonSetBaudRate.TabIndex = 1;
            this.buttonSetBaudRate.Text = "Set";
            this.buttonSetBaudRate.UseVisualStyleBackColor = false;
            this.buttonSetBaudRate.Click += new System.EventHandler(this.buttonSetBaudRate_Click);
            // 
            // comboBoxSetBaudRate
            // 
            this.comboBoxSetBaudRate.FormattingEnabled = true;
            this.comboBoxSetBaudRate.Items.AddRange(new object[] {
            "9600bps",
            "19200bps",
            "38400bps",
            "57600bps",
            "115200bps"});
            this.comboBoxSetBaudRate.Location = new System.Drawing.Point(89, 22);
            this.comboBoxSetBaudRate.Name = "comboBoxSetBaudRate";
            this.comboBoxSetBaudRate.Size = new System.Drawing.Size(121, 24);
            this.comboBoxSetBaudRate.TabIndex = 2;
            // 
            // groupBoxDBM
            // 
            this.groupBoxDBM.Controls.Add(this.buttonSetBaudRate);
            this.groupBoxDBM.Controls.Add(this.comboBoxSetBaudRate);
            this.groupBoxDBM.Enabled = false;
            this.groupBoxDBM.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxDBM.Location = new System.Drawing.Point(346, 155);
            this.groupBoxDBM.Name = "groupBoxDBM";
            this.groupBoxDBM.Size = new System.Drawing.Size(324, 60);
            this.groupBoxDBM.TabIndex = 4;
            this.groupBoxDBM.TabStop = false;
            this.groupBoxDBM.Text = "RS232/485 baud rate";
            // 
            // buttonSetRegion
            // 
            this.buttonSetRegion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonSetRegion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetRegion.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonSetRegion.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonSetRegion.Location = new System.Drawing.Point(550, 51);
            this.buttonSetRegion.Name = "buttonSetRegion";
            this.buttonSetRegion.Size = new System.Drawing.Size(96, 23);
            this.buttonSetRegion.TabIndex = 1;
            this.buttonSetRegion.Text = "Set";
            this.buttonSetRegion.UseVisualStyleBackColor = false;
            this.buttonSetRegion.Click += new System.EventHandler(this.buttonSetRegion_Click);
            // 
            // labelMinfre
            // 
            this.labelMinfre.AutoSize = true;
            this.labelMinfre.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelMinfre.Location = new System.Drawing.Point(339, 24);
            this.labelMinfre.Name = "labelMinfre";
            this.labelMinfre.Size = new System.Drawing.Size(50, 17);
            this.labelMinfre.TabIndex = 3;
            this.labelMinfre.Text = "Minfre:";
            // 
            // comboBoxDminfre
            // 
            this.comboBoxDminfre.FormattingEnabled = true;
            this.comboBoxDminfre.Location = new System.Drawing.Point(409, 17);
            this.comboBoxDminfre.Name = "comboBoxDminfre";
            this.comboBoxDminfre.Size = new System.Drawing.Size(121, 24);
            this.comboBoxDminfre.TabIndex = 2;
            // 
            // comboBoxDmaxfre
            // 
            this.comboBoxDmaxfre.FormattingEnabled = true;
            this.comboBoxDmaxfre.Location = new System.Drawing.Point(409, 50);
            this.comboBoxDmaxfre.Name = "comboBoxDmaxfre";
            this.comboBoxDmaxfre.Size = new System.Drawing.Size(121, 24);
            this.comboBoxDmaxfre.TabIndex = 2;
            // 
            // groupBoxRegion
            // 
            this.groupBoxRegion.Controls.Add(this.radioButtonEUband);
            this.groupBoxRegion.Controls.Add(this.radioButtonChineeseBand);
            this.groupBoxRegion.Controls.Add(this.radioButtonChineeseBand2);
            this.groupBoxRegion.Controls.Add(this.radioButtonUSBand);
            this.groupBoxRegion.Controls.Add(this.checkBoxRegionSingle);
            this.groupBoxRegion.Controls.Add(this.buttonSetRegion);
            this.groupBoxRegion.Controls.Add(this.labelMaxfre);
            this.groupBoxRegion.Controls.Add(this.labelMinfre);
            this.groupBoxRegion.Controls.Add(this.comboBoxDminfre);
            this.groupBoxRegion.Controls.Add(this.comboBoxDmaxfre);
            this.groupBoxRegion.Enabled = false;
            this.groupBoxRegion.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxRegion.Location = new System.Drawing.Point(12, 216);
            this.groupBoxRegion.Name = "groupBoxRegion";
            this.groupBoxRegion.Size = new System.Drawing.Size(658, 87);
            this.groupBoxRegion.TabIndex = 4;
            this.groupBoxRegion.TabStop = false;
            this.groupBoxRegion.Text = "Region";
            // 
            // radioButtonEUband
            // 
            this.radioButtonEUband.AutoSize = true;
            this.radioButtonEUband.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButtonEUband.Location = new System.Drawing.Point(225, 52);
            this.radioButtonEUband.Name = "radioButtonEUband";
            this.radioButtonEUband.Size = new System.Drawing.Size(80, 21);
            this.radioButtonEUband.TabIndex = 2;
            this.radioButtonEUband.TabStop = true;
            this.radioButtonEUband.Text = "EU band";
            this.radioButtonEUband.UseVisualStyleBackColor = true;
            this.radioButtonEUband.CheckedChanged += new System.EventHandler(this.radioButtonEUband_CheckedChanged);
            // 
            // radioButtonChineeseBand
            // 
            this.radioButtonChineeseBand.AutoSize = true;
            this.radioButtonChineeseBand.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButtonChineeseBand.Location = new System.Drawing.Point(46, 51);
            this.radioButtonChineeseBand.Name = "radioButtonChineeseBand";
            this.radioButtonChineeseBand.Size = new System.Drawing.Size(124, 21);
            this.radioButtonChineeseBand.TabIndex = 2;
            this.radioButtonChineeseBand.TabStop = true;
            this.radioButtonChineeseBand.Text = "Chineese band";
            this.radioButtonChineeseBand.UseVisualStyleBackColor = true;
            this.radioButtonChineeseBand.CheckedChanged += new System.EventHandler(this.radioButtonChineeseBand_CheckedChanged);
            // 
            // radioButtonChineeseBand2
            // 
            this.radioButtonChineeseBand2.AutoSize = true;
            this.radioButtonChineeseBand2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButtonChineeseBand2.Location = new System.Drawing.Point(46, 22);
            this.radioButtonChineeseBand2.Name = "radioButtonChineeseBand2";
            this.radioButtonChineeseBand2.Size = new System.Drawing.Size(131, 21);
            this.radioButtonChineeseBand2.TabIndex = 2;
            this.radioButtonChineeseBand2.TabStop = true;
            this.radioButtonChineeseBand2.Text = "Chineese band2";
            this.radioButtonChineeseBand2.UseVisualStyleBackColor = true;
            this.radioButtonChineeseBand2.CheckedChanged += new System.EventHandler(this.radioButtonChineeseBand2_CheckedChanged);
            // 
            // radioButtonUSBand
            // 
            this.radioButtonUSBand.AutoSize = true;
            this.radioButtonUSBand.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButtonUSBand.Location = new System.Drawing.Point(226, 22);
            this.radioButtonUSBand.Name = "radioButtonUSBand";
            this.radioButtonUSBand.Size = new System.Drawing.Size(79, 21);
            this.radioButtonUSBand.TabIndex = 2;
            this.radioButtonUSBand.TabStop = true;
            this.radioButtonUSBand.Text = "US band";
            this.radioButtonUSBand.UseVisualStyleBackColor = true;
            // 
            // checkBoxRegionSingle
            // 
            this.checkBoxRegionSingle.AutoSize = true;
            this.checkBoxRegionSingle.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.checkBoxRegionSingle.Location = new System.Drawing.Point(550, 19);
            this.checkBoxRegionSingle.Name = "checkBoxRegionSingle";
            this.checkBoxRegionSingle.Size = new System.Drawing.Size(64, 21);
            this.checkBoxRegionSingle.TabIndex = 2;
            this.checkBoxRegionSingle.Text = "Single";
            this.checkBoxRegionSingle.UseVisualStyleBackColor = true;
            // 
            // labelMaxfre
            // 
            this.labelMaxfre.AutoSize = true;
            this.labelMaxfre.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelMaxfre.Location = new System.Drawing.Point(338, 54);
            this.labelMaxfre.Name = "labelMaxfre";
            this.labelMaxfre.Size = new System.Drawing.Size(54, 17);
            this.labelMaxfre.TabIndex = 3;
            this.labelMaxfre.Text = "Maxfre:";
            // 
            // groupBoxBuffLength
            // 
            this.groupBoxBuffLength.Controls.Add(this.buttonGetBuffLength);
            this.groupBoxBuffLength.Controls.Add(this.radioButton496bitBuff);
            this.groupBoxBuffLength.Controls.Add(this.radioButton128bitBuff);
            this.groupBoxBuffLength.Controls.Add(this.buttonSetBuffLength);
            this.groupBoxBuffLength.Enabled = false;
            this.groupBoxBuffLength.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBoxBuffLength.Location = new System.Drawing.Point(12, 303);
            this.groupBoxBuffLength.Name = "groupBoxBuffLength";
            this.groupBoxBuffLength.Size = new System.Drawing.Size(210, 87);
            this.groupBoxBuffLength.TabIndex = 4;
            this.groupBoxBuffLength.TabStop = false;
            this.groupBoxBuffLength.Text = "Buffer EPC/TID length";
            // 
            // buttonGetBuffLength
            // 
            this.buttonGetBuffLength.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonGetBuffLength.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGetBuffLength.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonGetBuffLength.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonGetBuffLength.Location = new System.Drawing.Point(100, 52);
            this.buttonGetBuffLength.Name = "buttonGetBuffLength";
            this.buttonGetBuffLength.Size = new System.Drawing.Size(96, 23);
            this.buttonGetBuffLength.TabIndex = 1;
            this.buttonGetBuffLength.Text = "Get";
            this.buttonGetBuffLength.UseVisualStyleBackColor = false;
            this.buttonGetBuffLength.Click += new System.EventHandler(this.btnGetBuffLength_Click);
            // 
            // radioButton496bitBuff
            // 
            this.radioButton496bitBuff.AutoSize = true;
            this.radioButton496bitBuff.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButton496bitBuff.Location = new System.Drawing.Point(19, 51);
            this.radioButton496bitBuff.Name = "radioButton496bitBuff";
            this.radioButton496bitBuff.Size = new System.Drawing.Size(64, 21);
            this.radioButton496bitBuff.TabIndex = 2;
            this.radioButton496bitBuff.Text = "496bit";
            this.radioButton496bitBuff.UseVisualStyleBackColor = true;
            // 
            // radioButton128bitBuff
            // 
            this.radioButton128bitBuff.AutoSize = true;
            this.radioButton128bitBuff.Checked = true;
            this.radioButton128bitBuff.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButton128bitBuff.Location = new System.Drawing.Point(19, 24);
            this.radioButton128bitBuff.Name = "radioButton128bitBuff";
            this.radioButton128bitBuff.Size = new System.Drawing.Size(64, 21);
            this.radioButton128bitBuff.TabIndex = 2;
            this.radioButton128bitBuff.TabStop = true;
            this.radioButton128bitBuff.Text = "128bit";
            this.radioButton128bitBuff.UseVisualStyleBackColor = true;
            // 
            // buttonSetBuffLength
            // 
            this.buttonSetBuffLength.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonSetBuffLength.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetBuffLength.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonSetBuffLength.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonSetBuffLength.Location = new System.Drawing.Point(100, 23);
            this.buttonSetBuffLength.Name = "buttonSetBuffLength";
            this.buttonSetBuffLength.Size = new System.Drawing.Size(96, 23);
            this.buttonSetBuffLength.TabIndex = 1;
            this.buttonSetBuffLength.Text = "Set";
            this.buttonSetBuffLength.UseVisualStyleBackColor = false;
            this.buttonSetBuffLength.Click += new System.EventHandler(this.buttonSetBuffLength_Click);
            // 
            // richTextBoxLogs
            // 
            this.richTextBoxLogs.Location = new System.Drawing.Point(12, 428);
            this.richTextBoxLogs.Name = "richTextBoxLogs";
            this.richTextBoxLogs.Size = new System.Drawing.Size(658, 96);
            this.richTextBoxLogs.TabIndex = 5;
            this.richTextBoxLogs.Text = "";
            this.richTextBoxLogs.TextChanged += new System.EventHandler(this.richTextBoxLogs_TextChanged);
            // 
            // labelOperationRecords
            // 
            this.labelOperationRecords.AutoSize = true;
            this.labelOperationRecords.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelOperationRecords.Location = new System.Drawing.Point(31, 408);
            this.labelOperationRecords.Name = "labelOperationRecords";
            this.labelOperationRecords.Size = new System.Drawing.Size(129, 17);
            this.labelOperationRecords.TabIndex = 3;
            this.labelOperationRecords.Text = "Operation records:";
            this.labelOperationRecords.Click += new System.EventHandler(this.labelOperationRecords_Click);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this.buttonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefresh.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonRefresh.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonRefresh.Location = new System.Drawing.Point(562, 402);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(96, 23);
            this.buttonRefresh.TabIndex = 1;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // InitUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.labelOperationRecords);
            this.Controls.Add(this.richTextBoxLogs);
            this.Controls.Add(this.groupBoxRegion);
            this.Controls.Add(this.groupBoxDBM);
            this.Controls.Add(this.groupBoxPower);
            this.Controls.Add(this.groupBoxBuffLength);
            this.Controls.Add(this.groupBoxBeep);
            this.Controls.Add(this.groupBoxFirmwareVersion);
            this.Controls.Add(this.groupBoxGPIOOperation);
            this.Controls.Add(this.groupBoxRedaerSerialNumber);
            this.Controls.Add(this.groupBoxReaderAddress);
            this.Controls.Add(this.groupBoxRS232);
            this.Name = "InitUserControl";
            this.Size = new System.Drawing.Size(680, 536);
            this.Load += new System.EventHandler(this.InitUserControl_Load);
            this.groupBoxRS232.ResumeLayout(false);
            this.groupBoxRS232.PerformLayout();
            this.groupBoxReaderAddress.ResumeLayout(false);
            this.groupBoxReaderAddress.PerformLayout();
            this.groupBoxRedaerSerialNumber.ResumeLayout(false);
            this.groupBoxRedaerSerialNumber.PerformLayout();
            this.groupBoxGPIOOperation.ResumeLayout(false);
            this.groupBoxGPIOOperation.PerformLayout();
            this.groupBoxFirmwareVersion.ResumeLayout(false);
            this.groupBoxFirmwareVersion.PerformLayout();
            this.groupBoxBeep.ResumeLayout(false);
            this.groupBoxBeep.PerformLayout();
            this.groupBoxPower.ResumeLayout(false);
            this.groupBoxPower.PerformLayout();
            this.groupBoxDBM.ResumeLayout(false);
            this.groupBoxRegion.ResumeLayout(false);
            this.groupBoxRegion.PerformLayout();
            this.groupBoxBuffLength.ResumeLayout(false);
            this.groupBoxBuffLength.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnConnectReader;
        private System.Windows.Forms.Button btnDisconnectReader;
        private System.Windows.Forms.ComboBox comboBoxBaud;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.Label labelBaud;
        private System.Windows.Forms.GroupBox groupBoxRS232;
        private System.Windows.Forms.Button btnSetAdr;
        private System.Windows.Forms.GroupBox groupBoxReaderAddress;
        private System.Windows.Forms.TextBox textBoxReadAddress;
        private System.Windows.Forms.TextBox textBoxReaderSerialNumber;
        private System.Windows.Forms.Button buttonReadSerial;
        private System.Windows.Forms.GroupBox groupBoxRedaerSerialNumber;
        private System.Windows.Forms.Button buttonGPIOSet;
        private System.Windows.Forms.GroupBox groupBoxGPIOOperation;
        private System.Windows.Forms.CheckBox checkBoxOUT2;
        private System.Windows.Forms.CheckBox checkBoxOUT1;
        private System.Windows.Forms.Button buttonGPIOGet;
        private System.Windows.Forms.CheckBox checkBoxINT2;
        private System.Windows.Forms.CheckBox checkBoxINT1;
        private System.Windows.Forms.TextBox textBoxFirmwareVersion;
        private System.Windows.Forms.GroupBox groupBoxFirmwareVersion;
        private System.Windows.Forms.Button buttonBeepSet;
        private System.Windows.Forms.GroupBox groupBoxBeep;
        private System.Windows.Forms.RadioButton radioButtonBeepClose;
        private System.Windows.Forms.RadioButton radioButtonBeepOpen;
        private System.Windows.Forms.Button buttonPowerSet;
        private System.Windows.Forms.ComboBox comboBoxPower;
        private System.Windows.Forms.GroupBox groupBoxPower;
        private System.Windows.Forms.Label labeldBm;
        private System.Windows.Forms.Button buttonSetBaudRate;
        private System.Windows.Forms.ComboBox comboBoxSetBaudRate;
        private System.Windows.Forms.GroupBox groupBoxDBM;
        private System.Windows.Forms.Button buttonSetRegion;
        private System.Windows.Forms.Label labelMinfre;
        private System.Windows.Forms.ComboBox comboBoxDminfre;
        private System.Windows.Forms.ComboBox comboBoxDmaxfre;
        private System.Windows.Forms.GroupBox groupBoxRegion;
        private System.Windows.Forms.RadioButton radioButtonEUband;
        private System.Windows.Forms.RadioButton radioButtonChineeseBand;
        private System.Windows.Forms.RadioButton radioButtonChineeseBand2;
        private System.Windows.Forms.RadioButton radioButtonUSBand;
        private System.Windows.Forms.CheckBox checkBoxRegionSingle;
        private System.Windows.Forms.Label labelMaxfre;
        private System.Windows.Forms.GroupBox groupBoxBuffLength;
        private System.Windows.Forms.Button buttonGetBuffLength;
        private System.Windows.Forms.RadioButton radioButton496bitBuff;
        private System.Windows.Forms.RadioButton radioButton128bitBuff;
        private System.Windows.Forms.Button buttonSetBuffLength;
        private System.Windows.Forms.RichTextBox richTextBoxLogs;
        private System.Windows.Forms.Label labelOperationRecords;
        private System.Windows.Forms.Button buttonRefresh;
        internal System.Windows.Forms.ComboBox comboBoxCOM;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UHF;

namespace TimingSystemNCore
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
            regUserControl.Visible = false;
            raceUserControl.Visible = false;
            initUserControl.Visible = true;
        }

        private void btnInit_Click(object sender, EventArgs e)
        {
            regUserControl.Visible = false;
            raceUserControl.Visible = false;
            initUserControl.Visible = true;
        }

        private void btnReg_Click(object sender, EventArgs e)
        {
            if(RWDev.readerInit)
            {
                regUserControl.Visible = true;
                raceUserControl.Visible = false;
                initUserControl.Visible = false;
            }
        }

        private void btnRace_Click(object sender, EventArgs e)
        {
            if(RWDev.readerInit)
            {
                regUserControl.Visible = false;
                raceUserControl.Visible = true;
                initUserControl.Visible = false;
            }
        }

        private void regUserControl_Load_1(object sender, EventArgs e)
        {

        }

        private void initUserControl_Load(object sender, EventArgs e)
        {

        }

        private void userControl_Handler(object sender, EventArgs e)
        {

        }
    }
}
